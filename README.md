# Why?

To use a RPI 4B as a Gitlab Runner to setup a WordPress theme with composer and npm.

# Installation

On the Gitlab Runner:

1. Install Docker
1. Go to location of this repo's `Dockerfile`
1. Run `docker build -t rpi-node-php-composer .` (this builds the docker image)
1. To verify run `docker container run -ti rpi-node-php-composer /bin/sh` (this runs the image so you can run commands on it) 
1. Check installed versions (e.g. `node -v`)
