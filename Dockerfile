FROM php:7

RUN apt-get update -yq \
    && apt-get -yq install curl gnupg \
    && curl -sL https://deb.nodesource.com/setup_12.x | bash \
    && apt-get update -yq \
    && apt-get install -yq zip unzip nodejs

COPY --from=composer /usr/bin/composer /usr/bin/composer
